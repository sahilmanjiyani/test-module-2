import { BrowserModule } from '@angular/platform-browser';
import { NgModule, PipeTransform, Pipe } from '@angular/core';
import { FormsModule, NgModel } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ChangePipe } from './change.pipe';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    ChangePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
